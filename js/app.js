$(document).ready(function () {
    $(".language-selected").on('click', function () {
        if ($(".language-select").hasClass("hidden")) {
            $(".language-select").addClass("active").removeClass('hidden');
            $(".icon-select").addClass('show-icon').removeClass('hide-icon');
        } else {
            $(".language-select").removeClass("active").addClass("hidden");
            $(".language-select").addClass("hidden");
            $(".icon-select").removeClass('show-icon').addClass('hide-icon');
        }
    });

    $(".english-select").on('click', function () {
        if ($(this).parent().parent().find('.english').hasClass('hidden')) {
            $('.english').removeClass('hidden').addClass('active');
            $('.vietnamese').removeClass('active').addClass('hidden');
            $('.language-select').removeClass('active').addClass('hidden');
            $('.icon-select').removeClass('show-icon').addClass('hide-icon');
        }
        else {
            $('.language-select').removeClass('active').addClass('hidden');
            $('.icon-select').removeClass('show-icon').addClass('hide-icon');
        }
    });

    $(".vietnamese-select").on('click', function () {
        if ($(this).parent().parent().find('.vietnamese').hasClass('hidden')) {
            $(".vietnamese-select").parent().parent().find('.vietnamese').removeClass('hidden').addClass('active');
            $(".vietnamese-select").parent().parent().find('.english').removeClass('active').addClass('hidden');
            $(".vietnamese-select").parent().parent().find('.language-select').removeClass('active').addClass('hidden');
            $(".vietnamese-select").parent().parent().find('.icon-select').removeClass('show-icon').addClass('hide-icon');
        }
        else {
            $(".vietnamese-select").parent().parent().find('.language-select').removeClass('active').addClass('hidden');
            $(".vietnamese-select").parent().parent().find('.icon-select').removeClass('show-icon').addClass('hide-icon');
        }
    });

    $(".button-mobile").on('click', function () {
        $(".menu").addClass("menu-show");
    });

    $(".close").on('click', function () {
        $(".menu").removeClass('menu-show');
    });

    $(".icon-search").on('click', function () {
        $(".menu-mobile").addClass("hidden");
        $(".search").removeClass('invisible').removeClass('opacity-0').addClass('w-full');
        $(this).addClass('hidden');
    });

    $(".close-search").on('click', function () {
        $(".menu-mobile").removeClass('hidden');
        $(".icon-search").removeClass('hidden');
        $(".search").addClass('opacity-0');
        $(".search").addClass('invisible').addClass('opacity-0').removeClass('w-full');
    });

    $(".menu-parent").on('click', function () {
        $(".submenu-mobile").removeClass('invisible').addClass('visible');
        $(".submenu-mobile").addClass('menu-show');
        $(".menu").addClass('bg-blue-700');
    });

    $(".menu-child-primary").on('click', function () {
        $(".sub-menu-mobile").removeClass('invisible').addClass('visible');
        $(".sub-menu-mobile").addClass('menu-show');
    });

    //menu cấp 2
    $(".back_to_parent").on('click', function () {
        $('.submenu-mobile').removeClass('menu-show');
        $(".menu").removeClass('bg-blue-700');
    });


    $(".close-menu-primary").on('click', function () {
        $(".menu").removeClass('menu-show');
        $(".submenu-mobile").removeClass('menu-show');
        $(".button-mobile").removeClass('hidden');
        $('.menu-search').removeClass('hidden');
        $('.logo-menu').removeClass('hidden');
    });


    //menu cấp 3
    $(".back_to_parent-second").on('click', function () {
        $('.sub-menu-mobile').removeClass('menu-show');
        $(".menu").removeClass('bg-blue-700');
    });

    $(".close-menu-second").on('click', function () {
        $('.sub-menu-mobile').addClass('invisible').removeClass('visible');
        $(".menu").removeClass('menu-show');
        $(".submenu-mobile").removeClass('menu-show');
        $(".button-mobile").removeClass('hidden');
        $('.menu-search').removeClass('hidden');
        $('.logo-menu').removeClass('hidden');
    });


    $(".menu-child-secondary").on('click', function () {
        $(".sub-menu").removeClass('invisible').addClass('visible');
        $(".sub-menu").addClass('menu-show');
        $(".sub-menu").addClass('bg-blue-700');
    });


    $(".back_to_parent-tertiary").on('click', function () {
        $('.sub-menu').removeClass('menu-show');
        $(".sub-menu").removeClass('bg-blue-700');
    });

    $(".close-menu-tertiary").on('click', function () {
        $('.sub-menu').addClass('invisible').removeClass('visible');
        $(".menu").removeClass('menu-show');
        $(".submenu-mobile").removeClass('menu-show');
        $(".button-mobile").removeClass('hidden');
        $('.menu-search').removeClass('hidden');
        $('.logo-menu').removeClass('hidden');
    });

    $('.your-class').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ],
        prevArrow: false,
        nextArrow: false
    });

    $('#carousel').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
    });

    $('.slick-slide-swiper').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
    });

    $('.slide-carousel').slick({
        infinite: true,
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }
        ],
        prevArrow: "<button class='prev-slide'><img src='images/icon_preview.svg'></button>",
        nextArrow: "<button class='next-slide'><img src='images/icon_next.svg'></button>",
    });

    var btn = $('#button');

    $(window).scroll(function() {
      if ($(window).scrollTop() > 300) {
        btn.addClass('show');
      } else {
        btn.removeClass('show');
      }
    });
    
    btn.on('click', function(e) {
      e.preventDefault();
      $('html, body').animate({scrollTop:0}, '300');
    });
});


